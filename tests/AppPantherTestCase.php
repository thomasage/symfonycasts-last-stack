<?php
declare(strict_types=1);

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;
use Zenstruck\Browser\Test\HasBrowser;

abstract class AppPantherTestCase extends PantherTestCase
{
    use HasBrowser {
        pantherBrowser as parentPantherBrowser;
    }

    protected function pantherBrowser(
        array $options = [],
        array $kernelOptions = [],
        array $managerOptions = [],
    ): AppBrowser {
        return $this->parentPantherBrowser($options, $kernelOptions, $managerOptions);
    }
}
