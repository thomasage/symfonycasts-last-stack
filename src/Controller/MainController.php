<?php

namespace App\Controller;

use App\Repository\PlanetRepository;
use App\Repository\VoyageRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function homepage(
        VoyageRepository $voyageRepository,
        PlanetRepository $planetRepository,
        #[MapQueryParameter] int $page = 1,
        #[MapQueryParameter] string $sort = 'leaveAt',
        #[MapQueryParameter] string $sortDirection = 'ASC',
        #[MapQueryParameter] string $query = null,
        #[MapQueryParameter('planets', \FILTER_VALIDATE_INT)] array $searchPlanets = [],
    ): Response {
        $validSorts = ['purpose', 'leaveAt'];
        $sort = in_array($sort, $validSorts, true) ? $sort : 'leaveAt';

        $pager = Pagerfanta::createForCurrentPageWithMaxPerPage(
            adapter: new QueryAdapter(
                $voyageRepository->findBySearchQueryBuilder(
                    query: $query,
                    searchPlanets: $searchPlanets,
                    sort: $sort,
                    direction: $sortDirection
                )
            ),
            currentPage: $page,
            maxPerPage: 10
        );

        return $this->render('main/homepage.html.twig', [
            'planets' => $planetRepository->findAll(),
            'searchPlanets' => $searchPlanets,
            'sort' => $sort,
            'sortDirection' => $sortDirection,
            'voyages' => $pager,
        ]);
    }
}
