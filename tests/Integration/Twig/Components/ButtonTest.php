<?php
declare(strict_types=1);

namespace App\Tests\Integration\Twig\Components;

use App\Twig\Components\Button;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\UX\TwigComponent\Test\InteractsWithTwigComponents;

/**
 * @coversDefaultClass \App\Twig\Components\Button
 */
final class ButtonTest extends KernelTestCase
{
    use InteractsWithTwigComponents;

    public function testButtonRendersWithVariants(): void
    {
        $component = $this->mountTwigComponent(Button::class, [
            'variant' => 'success',
        ]);

        $this->assertInstanceOf(Button::class, $component);
        $this->assertSame('success', $component->variant);

        $rendered = $this->renderTwigComponent(Button::class, [
            'variant' => 'success',
        ], '<span>Click me!</span>');

        $this->assertSame('Click me!', $rendered->crawler()->filter('span')->text());
    }
}
