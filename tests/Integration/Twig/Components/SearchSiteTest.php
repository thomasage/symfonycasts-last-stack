<?php
declare(strict_types=1);

namespace App\Tests\Integration\Twig\Components;

use App\Factory\VoyageFactory;
use App\Twig\Components\SearchSite;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\UX\LiveComponent\Test\InteractsWithLiveComponents;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * @coversDefaultClass \App\Twig\Components\SearchSite
 */
final class SearchSiteTest extends KernelTestCase
{
    use Factories;
    use InteractsWithLiveComponents;
    use ResetDatabase;

    public function testCanRenderAndReload(): void
    {
        VoyageFactory::createMany(5, [
            'purpose' => 'first 5 voyages',
        ]);
        VoyageFactory::createOne();

        $testComponent = $this->createLiveComponent(SearchSite::class);

        $this->assertCount(0, $testComponent->render()->crawler()->filter('a'));

        $testComponent->set('query', 'first 5');

        $this->assertCount(5, $testComponent->render()->crawler()->filter('a'));
    }
}
